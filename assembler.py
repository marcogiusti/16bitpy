from collections.abc import Iterable
from enum import Enum, IntEnum, auto
from dataclasses import dataclass
import sys
from typing import cast

from vm16bit import (
    ARITHMETIC_MODE_SHIFT,
    ARITHMETIC_OPERATION_SHIFT,
    ARITHMETIC_SHIFT_SHIFT,
    DESTINATION_SHIFT,
    JUMP_OFFSET_SHIFT,
    JUMP_OPERATION_SHIFT,
    JUMP_REGISTER_SHIFT,
    LOAD_16_MASK,
    LOAD_16_SHIFT,
    HIGH10_SHIFT,
    MOVE_OPERATION_SHIFT,
    HIGH8_SHIFT,
    NO_ARG_OPERATION_SHIFT,
    SOURCE_SHIFT,
    STORE_VALUE_SHIFT,
    ArithmeticOperation,
    JumpOp,
    NoArgumentOpCode,
    OpCode,
    Register,
)


class CompileError(Exception):
    def __init__(self, msg: str, line_no: int = 0) -> None:
        self.msg = msg
        self.line_no = line_no

    def __str__(self) -> str:
        if self.line_no:
            return f"{self.line_no}: {self.msg}"
        return self.msg


class DataType(Enum):
    BUFFER = auto()
    INT = auto()
    STRING = auto()


@dataclass
class DataDeclaration:
    name: str
    type: DataType
    value: str | int
    size: int
    offset: int


@dataclass
class DataSection:
    name = "data"
    declarations: list[DataDeclaration]
    offset: int


@dataclass
class Instruction:
    name: str
    args: list[str]
    label: str | None = None
    line_no: int = 0

    def __str__(self) -> str:
        if self.label:
            return f"{self.label}: {self.name} {', '.join(self.args)}"
        else:
            return f"{self.name} {', '.join(self.args)}"


@dataclass
class TextSection:
    name = "text"
    instructions: list[Instruction]


@dataclass
class Program:
    data: DataSection
    text: TextSection
    labels_table: dict[str, int]


class UserRegister(IntEnum):
    A = Register.A
    B = Register.B
    C = Register.C
    D = Register.D


def escape(string: str) -> Iterable[int]:
    escape_map = {
        "n": "\n",
        "r": "\r",
        "f": "\f",
        "b": "\b",
        "t": "\t",
        "v": "\v",
    }
    escape_next = False
    istring = iter(string)
    for char in istring:
        if escape_next:
            escaped = escape_map.get(char, None)
            if escaped is not None:
                yield escaped
            else:
                yield "\\"
                yield char
            escape_next = False
        elif char == "\\":
            escape_next = True
        else:
            yield char


def parse(asm: str) -> Program:
    prog = Program(DataSection([], 0), TextSection([]), {})
    current_section: TextSection | DataSection | None = None
    current_label = None
    address = 0
    data_offset = 0

    for line_no, line in enumerate(asm.splitlines(), 1):
        comment_idx = line.find(";")
        if comment_idx >= 0:
            line = line[:comment_idx]
        line = line.strip()
        if not line:
            continue

        if line.startswith("."):
            name = line[1:]
            if name == "data":
                current_section = prog.data
            elif name == "text":
                current_section = prog.text
            else:
                raise ValueError(f"invalid section {name}")
        elif current_section is None:
            raise ValueError("invalid global section")
        elif current_section.name == "data":
            value: str | int
            match line.split(maxsplit=2):
                case [name, "size", value]:
                    type = DataType.BUFFER
                    value = int(value)
                    size = value
                case [name, "string", value]:
                    type = DataType.STRING
                    value = "".join(escape(value[1:-1]))
                    size = len(value) + 1
                case [name, value]:
                    type = DataType.INT
                    value = int(value)
                    size = 1
            declaration = DataDeclaration(name, type, value, size, data_offset)
            prog.data.declarations.append(declaration)
            data_offset += size
        else:
            match line.split(":", 1):
                case [label, ""]:
                    current_label = label.strip()
                    prog.labels_table[current_label] = address
                    code = ""
                case [label, code]:
                    current_label = label.strip()
                    prog.labels_table[current_label] = address
                    code = code.strip()
                case [code]:
                    code = code.strip()

            if not code:
                continue

            match code.split(maxsplit=1):
                case [name, args_str]:
                    args = [arg.strip() for arg in args_str.split(",")]
                case [name]:
                    args = []
            if name == "global":
                instruction1 = Instruction(
                    "MVV", ["A", args[0], "0"], label=current_label, line_no=line_no
                )
                instruction2 = Instruction("JMR", ["A"], line_no=line_no)
                prog.text.instructions.append(instruction1)
                prog.text.instructions.append(instruction2)
                current_label = None
                address += 2
            else:
                name = name.upper()
                instruction = Instruction(
                    name.upper(), args, label=current_label, line_no=line_no
                )
                prog.text.instructions.append(instruction)
                current_label = None
                if name == "LDV16":
                    address += 2
                if name == "SWP":
                    address += 3
                else:
                    address += 1

    prog.data.offset = len(prog.text.instructions)
    return prog


def _parse_int(s: str) -> int:
    neg = False
    if s.startswith("-"):
        s = s[1:]
        neg = True
    if s.startswith("0b"):
        value = int(s, 2)
    elif s.startswith("0o"):
        value = int(s, 8)
    elif s.startswith("0x"):
        value = int(s, 16)
    else:
        value = int(s)

    if neg:
        return -value
    else:
        return value


def parse_int(value: str, bits_no: int) -> int:
    try:
        i = _parse_int(value)
        if i // (1 << bits_no) > 0:
            raise CompileError(f"Invalid integer of {bits_no} bits, {value}")
        return i % (1 << bits_no)
    except ValueError:
        raise CompileError(f"Invalid integer {value}")


def register(value: str) -> int:
    try:
        return UserRegister(_parse_int(value))
    except ValueError:
        try:
            return UserRegister[value.upper()]
        except KeyError:
            raise CompileError(f"Invalid register {value}")


def arithmentic_operation(value: str) -> int:
    try:
        return ArithmeticOperation(_parse_int(value))
    except ValueError:
        try:
            return ArithmeticOperation[value.upper()]
        except KeyError:
            raise CompileError(f"Invalid operation {value}")


def jump_operation(value: str) -> int:
    try:
        return JumpOp(_parse_int(value))
    except ValueError:
        try:
            return JumpOp[value.upper()]
        except KeyError:
            raise CompileError(f"Invalid operation {value}")


def noa_operation(value: str) -> int:
    try:
        return NoArgumentOpCode(_parse_int(value))
    except ValueError:
        try:
            return NoArgumentOpCode[value.upper()]
        except KeyError:
            raise CompileError(f"Invalid operation {value}")


def _int(value: str, prog: Program, bits_no: int) -> int:
    try:
        return parse_int(value, bits_no)
    except CompileError:
        try:
            int_value = prog.labels_table[value]
            if int_value >= 1 << bits_no:
                raise CompileError(f"label points to an address that is too big {int_value} for {bits_no} bits")
            return int_value
        except KeyError:
            for decl in prog.data.declarations:
                if decl.name == value:
                    int_value = prog.data.offset + decl.offset
                    if int_value >= 1 << bits_no:
                        raise CompileError(f"data is in an address that is too big {int_value} for {bits_no} bits")
                    return int_value
            raise CompileError(f"Invalid value {value}")


def i8(value: str, prog: Program) -> int:
    return _int(value, prog, 8)


def memory(value: str, prog: Program) -> int:
    return _int(value, prog, 10)


def convert(instruction: Instruction) -> list[Instruction]:
    name = instruction.name
    args = instruction.args

    # arithmetic

    if name == "ADD":
        name = "ATH"
        args = [args[0], args[1], "0", "0", "0"]

    elif name == "ADDS":
        name = "ATH"
        args = [args[0], args[1], "0", "1", "0"]

    elif name == "ADI":
        name = "MVV"
        args = [args[0], args[1], "1"]

    elif name == "AUI":
        name = "MVV"
        args = [args[0], args[1], "3"]

    elif name == "DEC":
        name = "MVR"
        args = [args[0], args[0], "255"]

    elif name == "DIV":
        name = "ATH"
        args = [args[0], args[1], "3", "0", "0"]

    elif name == "DIVS":
        name = "ATH"
        args = [args[0], args[1], "3", "1", "0"]

    elif name == "INC":
        name = "MVR"
        args = [args[0], args[0], "1"]

    elif name == "LDV":
        name = "MVV"
        args = [args[0], args[1], "0"]

    elif name == "MOV":
        name = "MVR"
        args = [args[0], args[1], "0"]

    elif name == "MUI":
        name = "MVV"
        args = [args[0], args[1], "2"]

    elif name == "MUL":
        name = "ATH"
        args = [args[0], args[1], "2", "0", "0"]

    elif name == "MULS":
        name = "ATH"
        args = [args[0], args[1], "2", "1", "0"]

    elif name == "MVI":
        name = "MVV"
        args = [args[0], args[1], "0"]

    elif name == "SUB":
        name = "ATH"
        args = [args[0], args[1], "1", "0", "0"]

    elif name == "SUBS":
        name = "ATH"
        args = [args[0], args[1], "1", "1", "0"]

    # bitwise

    elif name == "AND":
        name = "ATH"
        args = [args[0], args[1], "8", "0", "0"]

    elif name == "LSF":
        name = "ATH"
        args = [args[0], "0", "6", "0", args[1]]

    elif name == "NOT":
        name = "ATH"
        args = [args[0], "0", "11", "0", "0"]

    elif name == "OR":
        name = "ATH"
        args = [args[0], args[1], "9", "0", "0"]

    elif name == "RSF":
        name = "ATH"
        args = [args[0], "0", "7", "0", args[1]]

    elif name == "XOR":
        name = "ATH"
        args = [args[0], args[1], "10", "0", "0"]

    # conditional

    elif name == "JEQ":
        name = "JCP"
        args = [args[0], args[1], args[2], "0"]

    elif name == "JGE":
        name = "JCP"
        args = [args[0], args[1], args[2], "5"]

    elif name == "JGT":
        name = "JCP"
        args = [args[0], args[1], args[2], "3"]

    elif name == "JLE":
        name = "JCP"
        args = [args[0], args[1], args[2], "4"]

    elif name == "JLT":
        name = "JCP"
        args = [args[0], args[1], args[2], "2"]

    elif name == "JNE":
        name = "JCP"
        args = [args[0], args[1], args[2], "1"]

    elif name == "JNZ":
        name = "JCP"
        args = [args[0], args[1], args[2], "7"]

    elif name == "JZE":
        name = "JCP"
        args = [args[0], args[1], args[2], "6"]

    # general

    elif name == "HLT":
        name = "NOA"
        args = ["3"]

    elif name == "LDV16":
        value = _parse_int(args[1])
        return [
            Instruction(name="MVI", args=[args[0], str(value & LOAD_16_MASK)]),
            Instruction(name="AUI", args=[args[0], str(value >> LOAD_16_SHIFT)]),
        ]

    elif name == "NOP":
        name = "NOA"
        args = ["0"]

    elif name == "RET":
        name = "NOA"
        args = ["1"]

    elif name == "SWP":
        return [
            Instruction(name="ATH", args=[args[0], args[1], "10", "0", "0"]),
            Instruction(name="ATH", args=[args[0], args[1], "10", "1", "0"]),
            Instruction(name="ATH", args=[args[0], args[1], "10", "0", "0"]),
        ]

    elif name == "SYS":
        name = "NOA"
        args = ["2"]

    # memory

    elif name == "LDM":
        name = "STA"
        args = [args[0], args[1]]

    elif name == "LDP":
        name = "STR"
        args = [args[0], args[1], "0"]

    else:
        return [instruction]

    return [Instruction(name=name, args=args)]


def encode(prog: Program, instruction: Instruction) -> int:
    name = instruction.name
    args = instruction.args

    if name == "MVR":
        dst = register(args[0])
        src = register(args[1])
        value = i8(args[2], prog)
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (src << SOURCE_SHIFT)
            + (value << HIGH8_SHIFT)
        )

    elif name == "MVV":
        dst = register(args[0])
        value = i8(args[1], prog)
        op = parse_int(args[2], 2)
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (op << MOVE_OPERATION_SHIFT)
            + (value << HIGH8_SHIFT)
        )

    elif name == "LDR":
        dst = register(args[0])
        src = register(args[1])
        value = i8(args[2], prog) if len(args) == 3 else 0
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (src << SOURCE_SHIFT)
            + (value << HIGH8_SHIFT)
        )

    elif name == "STA":
        dst = register(args[0])
        mem = memory(args[1], prog)
        byte_code = OpCode[name] + (dst << DESTINATION_SHIFT) + (mem << HIGH10_SHIFT)

    elif name == "ATH":
        dst = register(args[0])
        src = register(args[1])
        op = arithmentic_operation(args[2])
        mode = parse_int(args[3], 1)
        shift = parse_int(args[4], 3)
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (src << SOURCE_SHIFT)
            + (op << ARITHMETIC_OPERATION_SHIFT)
            + (mode << ARITHMETIC_MODE_SHIFT)
            + (shift << ARITHMETIC_SHIFT_SHIFT)
        )

    elif name == "CAL":
        dst = register(args[0])
        byte_code = OpCode[name] + (dst << DESTINATION_SHIFT)

    elif name == "JCP":
        dst = register(args[0])
        src = register(args[1])
        jmp_reg = register(args[2])
        op = jump_operation(args[3])
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (src << SOURCE_SHIFT)
            + (jmp_reg << JUMP_REGISTER_SHIFT)
            + (op << JUMP_OPERATION_SHIFT)
        )

    elif name == "PSH":
        src = register(args[0])
        byte_code = OpCode[name] + (src << SOURCE_SHIFT)

    elif name == "POP":
        dst = register(args[0])
        byte_code = OpCode[name] + (dst << DESTINATION_SHIFT)

    elif name == "JMP":
        jump_offset = parse_int(args[0], 12)
        byte_code = OpCode[name] + (jump_offset << JUMP_OFFSET_SHIFT)

    elif name == "JMR":
        src = register(args[0])
        byte_code = OpCode[name] + (src << SOURCE_SHIFT)

    elif name == "LDA":
        dst = register(args[0])
        mem = memory(args[1], prog)
        byte_code = OpCode[name] + (dst << DESTINATION_SHIFT) + (mem << HIGH10_SHIFT)

    elif name == "STR":
        dst = register(args[0])
        src = register(args[1])
        value = i8(args[2], prog) if len(args) == 3 else 0
        byte_code = (
            OpCode[name]
            + (dst << DESTINATION_SHIFT)
            + (src << SOURCE_SHIFT)
            + (value << STORE_VALUE_SHIFT)
        )

    elif name == "NOA":
        op = noa_operation(args[0])
        byte_code = OpCode[name] + (op << NO_ARG_OPERATION_SHIFT)

    assert byte_code < 2 ** 16, "Instruction too big"

    return byte_code


def compile_to_bytecode(prog: Program) -> list[int]:
    code = []
    for instruction in prog.text.instructions:
        # pseudo instructions
        try:
            new_instructions = convert(instruction)
        except IndexError:
            raise CompileError("Invalid number of arguments.", instruction.line_no)
        except Exception as exc:
            raise CompileError(str(exc), instruction.line_no)
        # bytecode
        for new_instruction in new_instructions:
            byte_code = encode(prog, new_instruction)
            code.append(byte_code)

    for decl in prog.data.declarations:
        if decl.type == DataType.BUFFER:
            code.extend([0] * decl.size)
        elif decl.type == DataType.INT:
            code.append(cast(int, decl.value))
        elif decl.type == DataType.STRING:
            code.extend(ord(c) for c in cast(str, decl.value))
            code.append(0)

    return code


def compile(asm: str) -> list[int]:
    prog = parse(asm)
    byte_code = compile_to_bytecode(prog)
    return byte_code


def main() -> int:
    with open(sys.argv[1]) as fp:
        asm = fp.read()
    try:
        code = compile(asm)
    except CompileError as exc:
        print(exc)
        return 1
    with open("a.out", "wb") as fp:
        for byte_code in code:
            fp.write(byte_code.to_bytes(length=2, byteorder="little"))
    return 0


if __name__ == "__main__":
    sys.exit(main())
