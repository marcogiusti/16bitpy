import os
import sys
from vm16bit import (
    ARITHMETIC_MODE_MASK,
    ARITHMETIC_MODE_SHIFT,
    ARITHMETIC_OPERATION_MASK,
    ARITHMETIC_OPERATION_SHIFT,
    ARITHMETIC_SHIFT_MASK,
    ARITHMETIC_SHIFT_SHIFT,
    CPU,
    DESTINATION_MASK,
    DESTINATION_SHIFT,
    HIGH10_MASK,
    HIGH10_SHIFT,
    HIGH8_MASK,
    HIGH8_SHIFT,
    JUMP_OFFSET_MASK,
    JUMP_OFFSET_SHIFT,
    JUMP_OPERATION_MASK,
    JUMP_OPERATION_SHIFT,
    JUMP_REGISTER_MASK,
    JUMP_REGISTER_SHIFT,
    MOVE_OPERATION_MASK,
    MOVE_OPERATION_SHIFT,
    NO_ARG_OPERATION_MASK,
    NO_ARG_OPERATION_SHIFT,
    OPCODE_MASK,
    SOURCE_MASK,
    SOURCE_SHIFT,
    ArithmeticMode,
    ArithmeticOperation,
    JumpOp,
    NoArgumentOpCode,
    OpCode,
    Register,
    read_char,
)
from assembler import Instruction


def clear() -> None:
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")


def bold(s: str) -> str:
    return f"\033[1m{s}\033[0m"


def decode(machine_code: int) -> Instruction:
    opcode = machine_code & OPCODE_MASK
    dst = (machine_code & DESTINATION_MASK) >> DESTINATION_SHIFT
    src = (machine_code & SOURCE_MASK) >> SOURCE_SHIFT
    high8 = (machine_code & HIGH8_MASK) >> HIGH8_SHIFT
    high10 = (machine_code & HIGH10_MASK) >> HIGH10_SHIFT

    name = OpCode(opcode).name
    args = []
    if opcode == OpCode.MVR:
        args = [Register(dst).name, Register(src).name, str(high8)]
    elif opcode == OpCode.MVV:
        move_operation = (machine_code & MOVE_OPERATION_MASK) >> MOVE_OPERATION_SHIFT
        args = [Register(dst).name, str(high8), str(move_operation)]
    elif opcode == OpCode.LDA:
        args = [Register(dst).name, str(high10)]
    elif opcode == OpCode.STA:
        args = [Register(dst).name, str(high10)]
    elif opcode == OpCode.LDR:
        args = [Register(dst).name, Register(src).name, str(high8)]
    elif opcode == OpCode.STR:
        args = [Register(dst).name, Register(src).name, str(high8)]
    elif opcode == OpCode.ATH:
        operation = (
            machine_code & ARITHMETIC_OPERATION_MASK
        ) >> ARITHMETIC_OPERATION_SHIFT
        mode = (machine_code & ARITHMETIC_MODE_MASK) >> ARITHMETIC_MODE_SHIFT
        shift = (machine_code & ARITHMETIC_SHIFT_MASK) >> ARITHMETIC_SHIFT_SHIFT
        args = [
            Register(dst).name,
            Register(src).name,
            ArithmeticOperation(operation).name,
            ArithmeticMode(mode).name,
            str(shift),
        ]
    elif opcode == OpCode.CAL:
        args = [Register(dst).name]
    elif opcode == OpCode.JCP:
        jump_reg = (machine_code & JUMP_REGISTER_MASK) >> JUMP_REGISTER_SHIFT
        jump_opcode = (machine_code & JUMP_OPERATION_MASK) >> JUMP_OPERATION_SHIFT
        args = [
            Register(dst).name,
            Register(src).name,
            JumpOp(jump_opcode).name,
            Register(jump_reg).name,
        ]
    elif opcode == OpCode.PSH:
        args = [Register(src).name]
    elif opcode == OpCode.POP:
        args = [Register(dst).name]
    elif opcode == OpCode.JMP:
        jump_offset = (machine_code & JUMP_OFFSET_MASK) >> JUMP_OFFSET_SHIFT
        args = [str(jump_offset)]
    elif opcode == OpCode.JMR:
        args = [Register(src).name]
    elif opcode == OpCode.NOA:
        no_arg_operation = (
            machine_code & NO_ARG_OPERATION_MASK
        ) >> NO_ARG_OPERATION_SHIFT
        name = NoArgumentOpCode(no_arg_operation).name
    return Instruction(name, args)


class Debugger:

    def __init__(self, cpu: CPU):
        self.cpu = cpu

    def run(self) -> None:
        memory_page = 0
        stop = False
        while not stop:
            self.render(self.cpu, memory_page)
            ch = read_char()
            if ch == "q":
                stop = True
            elif ch == "s":
                stop = self.cpu.step()
            elif ch == "n":
                memory_page = min(memory_page + 1, 31)
            elif ch == "p":
                memory_page = max(memory_page - 1, 0)

    def render(self, cpu: CPU, memory_page: int) -> None:
        memory = cpu.memory
        stack = cpu.stack
        registers = cpu.registers
        ip = registers[Register.IP]
        instruction = memory[ip]
        memory_offset = memory_page * 256

        clear()
        print("Memory:")
        for i in range(16):
            i *= 16
            addr = f"{memory_offset + i:0=4x}"
            print(f"{bold(addr)}   ", end="")
            for j in range(16):
                idx = memory_offset + i + j
                s = f"{memory[idx]:0=4x}"
                if idx == ip:
                    s = bold(s)
                print(f" {s}", end="")
            print()
        print(f"Page {memory_page + 1}/32")
        print()

        print("Stack:")
        for i in range(16):
            i *= 16
            addr = f"{i:0=4x}"
            print(f"{bold(addr)}   ", end="")
            for j in range(16):
                print(f" {stack[i + j]:0=4x}", end="")
            print()
        print()
        print(f"Instruction: {instruction:0=#18b}; {decode(instruction)}")
        print("Registers:")
        for reg in Register:
            print(
                f"{reg.name:>2}: "
                f"{registers[reg]:0=#18b} "
                f"{registers[reg]:>5} "
                f"{registers[reg]:0=#6x} "
                f"{repr(chr(registers[reg])):>6}"
            )
        print()
        print("(s)tep (q)uit (n)ext / (p)revious memory page >>>")


def main() -> None:
    machine_code = []
    with open(sys.argv[1], "rb") as fp:
        while instruction := fp.read(2):
            machine_code.append(int.from_bytes(instruction, byteorder="little"))
    cpu = CPU(frequency=0)
    cpu.load(machine_code)
    dbg = Debugger(cpu)
    dbg.run()


if __name__ == "__main__":
    main()
