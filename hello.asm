.text
  global main

main:
    MVI A, 0
    MVI B, 104 ; h
    MVI C, 3
    SYS
    MVI B, 101 ; e
    SYS
    MVI B, 108 ; l
    SYS
    MVI B, 108 ; l
    SYS
    MVI B, 111 ; o
    SYS
    MVI B, 32 ; <space>
    SYS
    MVI B, 119 ; w
    SYS
    MVI B, 111 ; o
    SYS
    MVI B, 114 ; r
    SYS
    MVI B, 108 ; l
    SYS
    MVI B, 100 ; d
    SYS
    MVI B, 10 ; \n
    SYS
    HLT
