from assembler import (
    CompileError,
    DataDeclaration,
    DataSection,
    DataType,
    Instruction,
    Program,
    TextSection,
    convert,
    encode,
    parse,
)

import pytest


def load(filename):
    with open(filename) as fp:
        return fp.read()


@pytest.mark.parametrize(
    "original, new_istructions",
    [
        # arithmetic
        (
            Instruction("ADD", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "0", "0", "0"])],
        ),
        (
            Instruction("ADDS", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "0", "1", "0"])],
        ),
        (
            Instruction("ADI", ["A", "12"]),
            [Instruction("MVV", ["A", "12", "1"])],
        ),
        (
            Instruction("AUI", ["A", "12"]),
            [Instruction("MVV", ["A", "12", "3"])],
        ),
        (
            Instruction("DEC", ["A"]),
            [Instruction("MVR", ["A", "A", "255"])],
        ),
        (
            Instruction("DIV", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "3", "0", "0"])],
        ),
        (
            Instruction("DIVS", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "3", "1", "0"])],
        ),
        (
            Instruction("INC", ["A"]),
            [Instruction("MVR", ["A", "A", "1"])],
        ),
        # (
        #     Instruction("LDV", ["A", "B"]),
        #     [Instruction("MVV", ["A", "B", "0"])],
        # ),
        (
            Instruction("MOV", ["A", "B"]),
            [Instruction("MVR", ["A", "B", "0"])],
        ),
        (
            Instruction("MUI", ["A", "42"]),
            [Instruction("MVV", ["A", "42", "2"])],
        ),
        (
            Instruction("MUL", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "2", "0", "0"])],
        ),
        (
            Instruction("MULS", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "2", "1", "0"])],
        ),
        (
            Instruction("MVI", ["A", "42"]),
            [Instruction("MVV", ["A", "42", "0"])],
        ),
        (
            Instruction("SUB", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "1", "0", "0"])],
        ),
        (
            Instruction("SUBS", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "1", "1", "0"])],
        ),
        # bitwise
        (
            Instruction("AND", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "8", "0", "0"])],
        ),
        (
            Instruction("LSF", ["A", "8"]),
            [Instruction("ATH", ["A", "0", "6", "0", "8"])],
        ),
        (
            Instruction("NOT", ["A"]),
            [Instruction("ATH", ["A", "0", "11", "0", "0"])],
        ),
        (
            Instruction("OR", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "9", "0", "0"])],
        ),
        (
            Instruction("RSF", ["A", "8"]),
            [Instruction("ATH", ["A", "0", "7", "0", "8"])],
        ),
        (
            Instruction("XOR", ["A", "B"]),
            [Instruction("ATH", ["A", "B", "10", "0", "0"])],
        ),
        # conditional
        (
            Instruction("JEQ", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "0"])],
        ),
        (
            Instruction("JGE", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "5"])],
        ),
        (
            Instruction("JGT", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "3"])],
        ),
        (
            Instruction("JLE", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "4"])],
        ),
        (
            Instruction("JLT", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "2"])],
        ),
        (
            Instruction("JNE", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "1"])],
        ),
        (
            Instruction("JNZ", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "7"])],
        ),
        (
            Instruction("JZE", ["A", "B", "123"]),
            [Instruction("JCP", ["A", "B", "123", "6"])],
        ),
        # general
        (
            Instruction("HLT", []),
            [Instruction("NOA", ["3"])],
        ),
        (
            Instruction("LDV16", ["A", "43605"]),
            [
                Instruction(name="MVI", args=["A", "85"]),
                Instruction(name="AUI", args=["A", "170"]),
            ],
        ),
        (
            Instruction("NOP", []),
            [Instruction("NOA", ["0"])],
        ),
        (
            Instruction("RET", []),
            [Instruction("NOA", ["1"])],
        ),
        (
            Instruction("SWP", ["A", "B"]),
            [
                Instruction(name="ATH", args=["A", "B", "10", "0", "0"]),
                Instruction(name="ATH", args=["A", "B", "10", "1", "0"]),
                Instruction(name="ATH", args=["A", "B", "10", "0", "0"]),
            ],
        ),
        (
            Instruction("SYS", []),
            [Instruction("NOA", ["2"])],
        ),
        # memory
        (
            Instruction("LDM", ["A", "B"]),
            [Instruction("STA", ["A", "B"])],
        ),
        (
            Instruction("LDP", ["A", "B"]),
            [Instruction("STR", ["A", "B", "0"])],
        ),
        # ignore any other command
        (Instruction("FOO", ["BAR", "SPAN"]), [Instruction("FOO", ["BAR", "SPAN"])]),
    ],
)
def test_convert(original, new_istructions):
    assert convert(original) == new_istructions


@pytest.mark.parametrize(
    "instruction, expected",
    [
        (Instruction("MVR", ["A", "B", "255"]), 0b1111_1111_0100_0000),
        (Instruction("MVR", ["A", "B", "-1"]), 0b1111_1111_0100_0000),
        (Instruction("MVV", ["A", "255", "0"]), 0b1111_1111_0000_0001),
        (Instruction("MVV", ["A", "255", "3"]), 0b1111_1111_1100_0001),
        (Instruction("MVV", ["A", "-1", "0"]), 0b1111_1111_0000_0001),
        (Instruction("LDR", ["A", "B", "255"]), 0b1111_1111_0100_0010),
        (Instruction("LDR", ["A", "B", "-1"]), 0b1111_1111_0100_0010),
        (Instruction("STA", ["A", "255"]), 0b0011_1111_1100_0011),
        (Instruction("STA", ["A", "-1"]), 0b1111_1111_1100_0011),
        (Instruction("ATH", ["A", "B", "AND", "1", "0"]), 0b0001_1000_0100_0100),
        (Instruction("CAL", ["B"]), 0b0000_0000_0001_0101),
        (Instruction("JCP", ["A", "B", "C", "LT"]), 0b0000_1010_0100_0110),
        (Instruction("PSH", ["C"]), 0b0000_0000_1000_0111),
        (Instruction("POP", ["C"]), 0b0000_0000_0010_1000),
        (Instruction("JMP", ["255"]), 0b0000_1111_1111_1001),
        (Instruction("JMR", ["D"]), 0b0000_0000_1100_1010),
        (Instruction("LDA", ["A", "1023"]), 0b1111_1111_1100_1011),
        (Instruction("STR", ["A", "B", "255"]), 0b1111_1111_0100_1100),
        (Instruction("STR", ["A", "B", "-1"]), 0b1111_1111_0100_1100),
        (Instruction("NOA", ["HLT"]), 0b0000_0000_0011_1101),
    ],
)
def test_encode(instruction, expected):
    prog = Program(DataSection([], 0), TextSection([]), {})
    assert encode(prog, instruction) == expected


@pytest.mark.parametrize(
    "instruction",
    [
        Instruction("MVR", ["A", "B", "65536"]),
        Instruction("MVV", ["A", "65536", "0"]),
        Instruction("LDR", ["A", "B", "65536"]),
        Instruction("STA", ["A", "65536"]),
        Instruction("JMP", ["4096"]),
        Instruction("STR", ["A", "B", "65536"]),
    ],
)
def test_encode_int_overflow(instruction):
    prog = Program(DataSection([], 0), TextSection([]), {})
    with pytest.raises(CompileError):
        encode(prog, instruction)


def test_simple():
    asm = """
    .text
        global _start

    _start:
        goto _start
    """

    expected = Program(
        DataSection([], 3),
        TextSection(
            [
                Instruction("MVV", ["A", "_start", "0"], line_no=3),
                Instruction("JMR", ["A"], line_no=3),
                Instruction("GOTO", ["_start"], label="_start", line_no=6),
            ],
        ),
        {"_start": 2},
    )
    assert parse(asm) == expected


def test_factorial():
    asm = load("factorial.asm")
    expected = Program(
        DataSection(
            [
                DataDeclaration("equals", DataType.STRING, " * 1 = ", 8, 0),
                DataDeclaration("counter", DataType.INT, 7, 1, 8),
                DataDeclaration("result", DataType.INT, 7, 1, 9),
                DataDeclaration("times", DataType.STRING, " * ", 4, 10),
            ],
            52,
        ),
        TextSection(
            [
                Instruction("MVV", ["A", "main", "0"], line_no=7),
                Instruction("JMR", ["A"], line_no=7),
                Instruction("LDA", ["A", "counter"], label="factorial", line_no=11),
                Instruction("PSH", ["A"], line_no=12),
                Instruction("MOV", ["B", "A"], line_no=15),
                Instruction("MVI", ["A", "0"], line_no=16),
                Instruction("MVI", ["C", "0"], line_no=17),
                Instruction("SYS", [], line_no=18),
                Instruction("POP", ["A"], line_no=20),
                Instruction("MVI", ["B", "3"], line_no=21),
                Instruction("MVI", ["D", "done"], line_no=22),
                Instruction("JLT", ["A", "B", "D"], line_no=23),
                Instruction("PSH", ["a"], line_no=25),
                Instruction("PSH", ["b"], line_no=26),
                Instruction("PSH", ["c"], line_no=27),
                Instruction("MVI", ["a", "0"], line_no=29),
                Instruction("MVI", ["b", "times"], line_no=30),
                Instruction("MVI", ["c", "4"], line_no=31),
                Instruction("SYS", [], line_no=32),
                Instruction("POP", ["c"], line_no=34),
                Instruction("POP", ["b"], line_no=35),
                Instruction("POP", ["a"], line_no=36),
                Instruction("MOV", ["B", "A"], line_no=38),
                Instruction("DEC", ["B"], line_no=39),
                Instruction("LDA", ["C", "result"], line_no=41),
                Instruction("MUL", ["C", "B"], line_no=42),
                Instruction("LDM", ["C", "result"], line_no=44),
                Instruction("LDM", ["B", "counter"], line_no=45),
                Instruction("MVI", ["A", "factorial"], line_no=47),
                Instruction("JMR", ["A"], line_no=48),
                Instruction("PSH", ["a"], label="done", line_no=51),
                Instruction("PSH", ["b"], line_no=52),
                Instruction("PSH", ["c"], line_no=53),
                Instruction("MVI", ["a", "0"], line_no=54),
                Instruction("MVI", ["b", "equals"], line_no=55),
                Instruction("MVI", ["c", "4"], line_no=56),
                Instruction("SYS", [], line_no=57),
                Instruction("POP", ["c"], line_no=58),
                Instruction("POP", ["b"], line_no=59),
                Instruction("POP", ["a"], line_no=60),
                Instruction("RET", [], line_no=61),
                Instruction("MVI", ["A", "factorial"], label="main", line_no=64),
                Instruction("CAL", ["A"], line_no=65),
                Instruction("MVI", ["A", "0"], line_no=68),
                Instruction("MVI", ["D", "result"], line_no=69),
                Instruction("LDR", ["B", "D"], line_no=70),
                Instruction("MVI", ["C", "0"], line_no=71),
                Instruction("SYS", [], line_no=72),
                Instruction("MVI", ["B", "10"], line_no=75),
                Instruction("MVI", ["C", "3"], line_no=76),
                Instruction("SYS", [], line_no=77),
                Instruction("HLT", [], line_no=78),
            ],
        ),
        {
            "factorial": 2,
            "done": 30,
            "main": 41,
        },
    )
    assert parse(asm) == expected
