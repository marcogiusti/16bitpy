from __future__ import annotations
from dataclasses import dataclass, field
from enum import IntEnum
import sys
import time


OPCODE_MASK                 = 0b0000000000001111
DESTINATION_MASK            = 0b0000000000110000
SOURCE_MASK                 = 0b0000000011000000
HIGH8_MASK                  = 0b1111111100000000
HIGH10_MASK                 = 0b1111111111000000

ARITHMETIC_OPERATION_MASK   = 0b0000111100000000
ARITHMETIC_MODE_MASK        = 0b0001000000000000
ARITHMETIC_SHIFT_MASK       = 0b1110000000000000
NO_ARG_OPERATION_MASK       = 0b0000000011110000
JUMP_OFFSET_MASK            = 0b1111111111110000
JUMP_REGISTER_MASK          = 0b0000001100000000
JUMP_OPERATION_MASK         = 0b0000110000000000
MOVE_OPERATION_MASK         = 0b0000000011000000
LOAD_16_MASK                = 0b0000000011111111

DESTINATION_SHIFT           = 4
SOURCE_SHIFT                = 6
HIGH8_SHIFT                 = 8
HIGH10_SHIFT                = 6

ARITHMETIC_OPERATION_SHIFT  = 8
ARITHMETIC_MODE_SHIFT       = 12
ARITHMETIC_SHIFT_SHIFT      = 13
NO_ARG_OPERATION_SHIFT      = 4
JUMP_OFFSET_SHIFT           = 4
JUMP_REGISTER_SHIFT         = 8
JUMP_OPERATION_SHIFT        = 10
STORE_VALUE_SHIFT           = 8
MOVE_OPERATION_SHIFT        = 6
LOAD_16_SHIFT               = 8


class UnknownOpcode(Exception):
    pass


class StackOverflow(Exception):
    pass


class EmptyStack(Exception):
    pass


class OpCode(IntEnum):
    MVR = 0b0000
    MVV = 0b0001
    LDR = 0b0010
    STA = 0b0011
    ATH = 0b0100
    CAL = 0b0101
    JCP = 0b0110
    PSH = 0b0111
    POP = 0b1000
    JMP = 0b1001
    JMR = 0b1010
    LDA = 0b1011
    STR = 0b1100
    NOA = 0b1101


class Register(IntEnum):
    A = 0
    B = 1
    C = 2
    D = 3
    IP = 4
    SP = 5


class MoveValueOpCode(IntEnum):
    MVI = 0b00
    ADI = 0b01
    MUI = 0b10
    AUI = 0b11


class ArithmeticOperation(IntEnum):
    ADD = 0b0000
    SUB = 0b0001
    MUL = 0b0010
    DIV = 0b0011
    INC = 0b0100
    DEC = 0b0101
    LSF = 0b0110
    RSF = 0b0111
    AND = 0b1000
    OR = 0b1001
    XOR = 0b1010
    NOT = 0b1011


class ArithmeticMode(IntEnum):
    DST = 0
    SRC = 1

    DESTINATION = DST
    SOURCE = SRC


class NoArgumentOpCode(IntEnum):
    NOP = 0b00
    RET = 0b01
    SYS = 0b10
    HLT = 0b11

    NO_OPERATION = NOP
    RETURN = RET
    SYSTEM_CALL = SYS
    HALT = HLT


class JumpOp(IntEnum):
    EQ = 0b000
    NE = 0b001
    LT = 0b010
    GT = 0b011
    LTE = 0b100
    GTE = 0b101
    ZE = 0b110
    NZ = 0b111

    EQUAL = EQ
    NOT_EQUAL = NE
    LESS_THAN = LT
    GREATER_THAN = GT
    LESS_THAN_OR_EQUAL = LTE
    GREATER_THAN_OR_EQUAL = GTE
    ZERO = ZE
    NOT_ZERO = NZ


class SystemCall(IntEnum):
    STDOUT = 0
    STDIN = 1


class StdoutMode(IntEnum):
    BIN = 1
    HEX = 2
    CHR = 3
    STR = 4


def _read_char_win() -> str:
    import msvcrt
    return msvcrt.getch()


def _read_char_unix() -> str:
    import tty, sys, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(fd)
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


def read_char() -> str:
    try:
        return _read_char_win()
    except ImportError:
        return _read_char_unix()


def twos_complement(i: int, bits: int) -> int:
    if (i & (1 << (bits - 1))) != 0:
        i = i - (1 << bits)
    return i

def mod(i: int, value: int = 1 << 16) -> int:
    return i % value


def alu(
    registers: list[int],
    dst: int,
    src: int,
    operation: int,
    mode: int,
    shift_positions: int,
) -> None:
    a = registers[dst]
    b = registers[src]
    if operation == ArithmeticOperation.ADD:
        result = a + b
    elif operation == ArithmeticOperation.SUB:
        result = a - b
    elif operation == ArithmeticOperation.MUL:
        result = a * b
    elif operation == ArithmeticOperation.DIV:
        result = a // b
    elif operation == ArithmeticOperation.INC:
        result = a + 1
    elif operation == ArithmeticOperation.DEC:
        result = a - 1
    elif operation == ArithmeticOperation.LSF:
        result = a << shift_positions
    elif operation == ArithmeticOperation.RSF:
        result = a >> shift_positions
    elif operation == ArithmeticOperation.AND:
        result = a & b
    elif operation == ArithmeticOperation.OR:
        result = a | b
    elif operation == ArithmeticOperation.XOR:
        result = a ^ b
    elif operation == ArithmeticOperation.NOT:
        result = ~b

    if mode == ArithmeticMode.DESTINATION:
        registers[dst] = mod(result)
    else:
        registers[src] = mod(result)


MEMORY_SIZE = 2 ** 13  # 8 KB
STACK_SIZE = 2 ** 10  # 1 KB


@dataclass
class CPU:
    registers: list[int] = field(default_factory=lambda: [0, 0, 0, 0, 0, 0])
    memory: list[int] = field(default_factory=lambda: [0] * MEMORY_SIZE)
    stack: list[int] = field(default_factory=lambda: [0] * STACK_SIZE)
    frequency: int = 0

    def reset(self) -> None:
        self.registers[:] = [0, 0, 0, 0, 0, 0]
        self.memory[:] = [0] * MEMORY_SIZE
        self.stack[:] = [0] * STACK_SIZE

    def load(self, instructions: list[int]) -> None:
        prog_len = len(instructions)
        mem_len = len(self.memory)
        assert prog_len <= mem_len
        self.memory[:prog_len] = instructions
        self.memory[prog_len:mem_len] = [0] * (mem_len - prog_len)

    def step(self) -> bool:
        instruction = self.fetch()
        stop = self.execute(instruction)
        return stop

    def run(self) -> None:
        t = 1 / self.frequency if self.frequency else 0
        stop = False
        while not stop:
            stop = self.step()
            if t:
                time.sleep(t)

    def fetch(self) -> int:
        address = self.registers[Register.IP]
        instruction = self.memory[address]
        self.registers[Register.IP] = address + 1
        return instruction

    def execute(self, machine_code: int) -> bool:
        registers = self.registers
        mem = self.memory
        stack = self.stack

        opcode = machine_code & OPCODE_MASK
        dst = (machine_code & DESTINATION_MASK) >> DESTINATION_SHIFT
        src = (machine_code & SOURCE_MASK) >> SOURCE_SHIFT
        high8 = (machine_code & HIGH8_MASK) >> HIGH8_SHIFT
        high10 = (machine_code & HIGH10_MASK) >> HIGH10_SHIFT

        if opcode == OpCode.MVR:
            registers[dst] = (registers[src] + twos_complement(high8, 8)) % (1 << 16)
        elif opcode == OpCode.MVV:
            move_operation = (machine_code & MOVE_OPERATION_MASK) >> MOVE_OPERATION_SHIFT
            if move_operation == MoveValueOpCode.MVI:
                registers[dst] = high8
            elif move_operation == MoveValueOpCode.ADI:
                registers[dst] = mod(registers[dst] + high8)
            elif move_operation == MoveValueOpCode.MUI:
                registers[dst] = mod(high8 << 8)
            elif move_operation == MoveValueOpCode.AUI:
                registers[dst] = mod(registers[dst] + high8 << 8)
        elif opcode == OpCode.LDA:
            registers[dst] = mem[high10]
        elif opcode == OpCode.STA:
            mem[high10] = registers[dst]
        elif opcode == OpCode.LDR:
            address = mod(registers[src] + high8)
            registers[dst] = mem[address]
        elif opcode == OpCode.STR:
            address = mod(registers[dst] + high8)
            mem[address] = registers[dst]
        elif opcode == OpCode.ATH:
            operation = (machine_code & ARITHMETIC_OPERATION_MASK) >> ARITHMETIC_OPERATION_SHIFT
            mode = (machine_code & ARITHMETIC_MODE_MASK) >> ARITHMETIC_MODE_SHIFT
            shift = (machine_code & ARITHMETIC_SHIFT_MASK) >> ARITHMETIC_SHIFT_SHIFT
            alu(registers, dst, src, operation, mode, shift)
        elif opcode == OpCode.CAL:
            stack[registers[Register.SP]] = registers[Register.IP]
            registers[Register.SP] = mod(registers[Register.SP] + 1)
            registers[Register.IP] = registers[dst]
        elif opcode == OpCode.JCP:
            jump_reg = (machine_code & JUMP_REGISTER_MASK) >> JUMP_REGISTER_SHIFT
            jump_opcode = (machine_code & JUMP_OPERATION_MASK) >> JUMP_OPERATION_SHIFT
            jump_flag = False
            if jump_opcode == JumpOp.EQ:
                jump_flag = registers[dst] == registers[src]
            if jump_opcode == JumpOp.NE:
                jump_flag = registers[dst] != registers[src]
            elif jump_opcode == JumpOp.LT:
                jump_flag = registers[dst] < registers[src]
            elif jump_opcode == JumpOp.GT:
                jump_flag = registers[dst] > registers[src]
            elif jump_opcode == JumpOp.LTE:
                jump_flag = registers[dst] <= registers[src]
            elif jump_opcode == JumpOp.GTE:
                jump_flag = registers[dst] >= registers[src]
            elif jump_opcode == JumpOp.ZE:
                jump_flag = registers[dst] == 0
            elif jump_opcode == JumpOp.NZ:
                jump_flag = registers[dst] != 0
            if jump_flag:
                registers[Register.IP] = registers[jump_reg]
        elif opcode == OpCode.PSH:
            stack[registers[Register.SP]] = registers[src]
            registers[Register.SP] = mod(registers[Register.SP] + 1)
        elif opcode == OpCode.POP:
            registers[Register.SP] = mod(registers[Register.SP] - 1)
            registers[dst] = stack[registers[Register.SP]]
        elif opcode == OpCode.JMP:
            jump_offset = (machine_code & JUMP_OFFSET_MASK) >> JUMP_OFFSET_SHIFT
            registers[Register.IP] = registers[Register.IP] + (-(jump_offset & 0x800) | (jump_offset & ~0x800))
        elif opcode == OpCode.JMR:
            registers[Register.IP] = registers[dst]
        elif opcode == OpCode.NOA:
            no_arg_operation = (machine_code & NO_ARG_OPERATION_MASK) >> NO_ARG_OPERATION_SHIFT
            if no_arg_operation == NoArgumentOpCode.NOP:
                pass
            elif no_arg_operation == NoArgumentOpCode.RET:
                registers[Register.SP] = mod(registers[Register.SP] - 1)
                registers[Register.IP] = stack[registers[Register.SP]]
            elif no_arg_operation == NoArgumentOpCode.SYS:
                if registers[Register.A] == SystemCall.STDOUT:
                    value = registers[Register.B]
                    mode = registers[Register.C]
                    if mode == StdoutMode.BIN:
                        out = bin(value)
                    elif mode == StdoutMode.HEX:
                        out = hex(value)
                    elif mode == StdoutMode.CHR:
                        out = chr(value)
                    elif mode == StdoutMode.STR:
                        buf = []
                        address = value
                        while c := mem[address]:
                            buf.append(chr(c))
                            address += 1
                        out = "".join(buf)
                    else:
                        out = str(value)
                    sys.stdout.write(out)
                    sys.stdout.flush()
                # elif registers[Register.A] == SystemCall.STDIN:
                #     registers[Register.B] = read_char()
            elif no_arg_operation == NoArgumentOpCode.HLT:
                return True

        return False


def run(machine_code: list[int], frequency: int) -> None:
    cpu = CPU(frequency=frequency)
    cpu.load(machine_code)
    cpu.run()


def main() -> None:
    machine_code = []
    with open(sys.argv[1], "rb") as fp:
        while instruction := fp.read(2):
            machine_code.append(int.from_bytes(instruction, byteorder="little"))
    run(machine_code, 0)


if __name__ == "__main__":
    main()
